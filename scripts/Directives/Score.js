"use strict";

cosmote_game.directive("scoreDir", ["ScoreService", "CategoryService", "$http", "URLService", "$location", "$window", function (ScoreService, CategoryService, $http, URLService, $location, $window) {
    return {
        restrict: "AE",
        templateUrl: "templates/Score.html",
        link: function (scope) {
            jQuery(document).ready(function () {
                jQuery("input, textarea").placeholder({
                    customClass: "custom-placeholder"
                });
            });
            scope.score = ScoreService.getScore();
            scope.createUserError = false;
            scope.bestScores = [];
            scope.restartGame = function () {
                ScoreService.setScore(0);
                ScoreService.clearQuestionsAnswers();
                CategoryService.resetCategoryStatus();
                $location.path("/categories");
            };
            scope.getScores = function () {
                var url = URLService.getUrl() + "addUser.php?format=json&email=" + ScoreService.getEmail() + "&tok=" + tok;
                var url2 = URLService.getUrl() + "setScore.php?email=" + ScoreService.getEmail() + "&score=" + ScoreService.getScore() + "&qa=" + JSON.stringify(ScoreService.getQuestionsAnswers()) + "&tok=" + tok;
                $.ajax({
                    url: url,
                    dataType: "json",
                    success: function (response) {
                        console.log(JSON.stringify(response));
                        if (response.status === "success") {
                            $.ajax({
                                url: url2,
                                dataType: "json",
                                success: function (response) {
                                    if (response.status === "failure") {
                                        scope.createUserError = true;
                                    }
                                    else {
                                        ScoreService.setPlaying(false);
                                        scope.createUserError = false;
                                        scope.bestScores = response.scores;
                                        scope.$apply();
                                    }
                                },
                                error: function (error) {
                                    scope.createUserError = true;
                                }
                            });
                        } else {
                            scope.createUserError = true;
                            scope.$apply();
                        }
                    },
                    error: function (error) {
                        scope.createUserError = true;
                    }
                });
            };
            if (ScoreService.getPlaying() === true) {
                scope.getScores();
            }
            else {
                $location.path("/");
            }

            scope.finishGame = function () {
                $location.path("/registration");
            };
        }
    };
}]);
/**
 * Created by cpad on 28/4/2017.
 */
"use strict";
var currentIndex;
Chart.defaults.global.colours = [{ // orange
    fillColor: "#43AD48",
    strokeColor: "#43AD48",
    pointColor: "#43AD48",
    pointStrokeColor: "#fff",
    pointHighlightFill: "#fff",
    pointHighlightStroke: "#43AD48"
}, { // yellow
    fillColor: "#F89E3E",
    strokeColor: "#F89E3E",
    pointColor: "#F89E3E",
    pointStrokeColor: "#fff",
    pointHighlightFill: "#fff",
    pointHighlightStroke: "#F89E3E"
}, { // green
    fillColor: "#F72213",
    strokeColor: "#F72213",
    pointColor: "#F72213",
    pointStrokeColor: "#fff",
    pointHighlightFill: "#fff",
    pointHighlightStroke: "#F72213"
}, { // white
    fillColor: "rgba(255,255,255,1)",
    strokeColor: "rgba(255,255,255,1)",
    pointColor: "rgba(255,255,255,1)",
    pointStrokeColor: "#fff",
    pointHighlightFill: "#fff",
    pointHighlightStroke: "rgba(255,255,255,1)"
}];

cosmote_game.directive("questionDir", ["ScoreService", "CategoryService", "$interval", "$location",
    function (ScoreService, CategoryService, $interval, $location) {
        return {
            templateUrl: "templates/Question.html",
            scope: {
                question: "=",
                score: "=",
                // timeLeft: "@start",
                userEmail: "@end"
            },
            link: function (scope) {
                scope.icon_url = '../../images/categories/' + CategoryService.getCategory() + '.png';
                jQuery(document).ready(function () {
                    jQuery("input, textarea").placeholder({
                        customClass: "custom-placeholder"
                    });
                });
                scope.QuestionsAnswers = [];
                scope.selectedAnswer = null;
                scope.selectedAnswerIndex = undefined;
                scope.currentValue = 0;
                scope.options = {
                    animation: true,
                    animationSteps: 60,
                    showTooltips: false,
                    percentageInnerCutout: 88,
                    animationEasing: "easeOutQuart",
                    segmentShowStroke: false,
                    responsive: false
                };
                // scope.showSubmitButton = true;

                scope.setSelected = function (answer, index) {
                    scope.selectedAnswer = answer;
                    scope.selectedAnswerIndex = index;
                    $(".answer").removeClass("selected");
                    $("#answer" + index).addClass("selected");
                    $(".main-button").addClass("button-enabled");
                };
                scope.selectAnswer = function () {
                    if (scope.selectedAnswer === null) return;
                    // scope.showSubmitButton = false;
                    scope.qa = ScoreService.getQuestionsAnswers();
                    scope.qa.push({category: CategoryService.getCategory(), answer: scope.selectedAnswer.answer_number, question: scope.question.question_id});
                    ScoreService.setQuestionsAnswers(scope.qa);
                    if (scope.selectedAnswer.right) {
                        var currentScore = ScoreService.getScore();
                        ScoreService.setScore(currentScore + 1);
                    }
                    if (CategoryService.allCategoriesAreAnswered()) {
                        $location.path("/score");
                    } else {
                        $location.path("/categories");
                    }
                };
            }
        };
    }
]);
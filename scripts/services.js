/**
 * Created by cpad on 28/4/2017.
 */
"use strict";

cosmote_game.factory("URLService", function () {
    // var url = "http://ws.otegroup-internalcommunication.gr/";
    var url = "http://192.168.1.142:8078/";
    return {
        getUrl: function () {
            return url;
        }
    };
});

cosmote_game.factory("ScoreService", function () {
    var score = 0;
    var userEmail;
    var playing = false;
    var questionsAnswers = [];
    return {
        setQuestionsAnswers: function (array) {
            questionsAnswers = array;
        },
        getQuestionsAnswers: function () {
            return questionsAnswers;
        },
        clearQuestionsAnswers: function () {
            questionsAnswers = [];
        },
        setPlaying: function (status) {
            playing = status;
        },
        getPlaying: function () {
            return playing;
        },
        getScore: function () {
            return score;
        },
        setScore: function (value) {
            score = value;
        },
        getEmail: function () {
            return userEmail;
        },
        setEmail: function (value) {
            userEmail = value;
        }
    };
});


cosmote_game.factory("CategoryService", function () {
    var category;
    var categoryStatus = [false, false, false, false];
    return {
        getCategory: function () {
            return category;
        },
        setCategory: function (value) {
            category = value;
        },
        getCategoryStatus: function () {
            return categoryStatus;
        },
        setCategoryStatus: function (value) {
            categoryStatus = value;
        },
        allCategoriesAreAnswered: function () {
            var status = categoryStatus[0];
            for (var cat = 1; cat < categoryStatus.length; cat++) {
                status = status && categoryStatus[cat];
            }
            return status;
        },
        resetCategoryStatus: function () {
            categoryStatus = [false, false, false, false];
        }
    };
});


cosmote_game.factory("authentication", function () {
    var logged = false;

    return {
        isLogged: function () {
            return logged;
            // return true;
        },
        login: function () {
            logged = true;
        },
        logout: function () {
            logged = false;
        }
    };
});


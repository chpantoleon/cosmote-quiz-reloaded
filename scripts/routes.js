/**
 * Created by cpad on 28/4/2017.
 */
"use strict";

cosmote_game.config(["$routeProvider", "$httpProvider", "$locationProvider", "$provide", function ($routeProvider, $httpProvider) {
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.withCredentials = true;
    $routeProvider
        .when("/", {
            templateUrl: "views/index.html",
            controller: "IndexCtrl",
            controllerAs: "index"
        })
        .when("/categories", {
            templateUrl: "views/categories.html",
            controller: "CategoryCtrl",
            controllerAs: "category"
        })
        .when("/game", {
            templateUrl: "views/game.html",
            controller: "GameCtrl",
            controllerAs: "main"
        })
        .when("/score", {
            templateUrl: "views/score.html",
            controller: "ScoreCtrl",
            controllerAs: "ScoreCtrl"
        })
        .when("/finished", {
            templateUrl: "views/finished.html",
            controller: "EndingCtrl",
            controllerAs: "ending"
        })
        .when("/registration", {
            templateUrl: "views/registration.html",
            controller: "RegistrationCtrl",
            controllerAs: "registration"
        })
        .when("/gifts", {
            templateUrl: "views/gifts.html",
            controller: "GiftsCtrl",
            controllerAs: "gifts"
        })
        .when("/terms", {
            templateUrl: "views/terms.html",
            controller: "TermsCtrl",
            controllerAs: "terms"
        })
        .otherwise({
            redirectTo: "/"
        });
}]);

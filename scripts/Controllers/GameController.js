/**
 * Created by cpad on 28/4/2017.
 */
"use strict";

cosmote_game.controller("GameCtrl", ["$scope", "ScoreService", "CategoryService", "authentication", "$location", function ($scope, ScoreService, CategoryService, authentication, $location) {
    jQuery(".ote-icon").show();
    jQuery(document).ready(function () {
        jQuery("input, textarea").placeholder({customClass: "custom-placeholder"});
    });
    if (!authentication.isLogged()) {
        $location.path("/");
    }
    ScoreService.setPlaying(true);
    $scope.formSubmit = function () {
        var email = $("#emailInput").val();
        if (isCosmoteEmail(email)) {
            if ($("#agreeCheckbox").is(":checked")) {
                ScoreService.setEmail(email);
                $location.path("/game");
            } else {
                setModalContent($("#basic-modal-content"), "checkbox");
                $("#basic-modal-content").modal();
            }
        } else {
            setModalContent($("#basic-modal-content"), "email");
            $("#basic-modal-content").modal();
        }
    };
    $scope.showTerms = function () {
        setModalContent($("#basic-modal-content"), "terms");
        $("#basic-modal-content").modal();
    };
    $scope.score = 0;
    $scope.currentQuestion = null;

    $scope.selectedCategory = CategoryService.getCategory();
    $scope.questions = [];
    $scope.questions['24support'] = [
        {
            question_id: 1,
            question: "Ποια είναι η διαδικασία για άρση φραγής στο σταθερό ή κινητό τηλέφωνο;",
            answers: [{
                answer_number: "A",
                answer: "Καλείς το 13888 και ζητάς να μιλήσεις με εκπρόσωπο για να ενημερωθείς για τη διαδικασία που πρέπει να ακολουθήσεις.",
                right: false
            }, {
                answer_number: "B",
                answer: "Καλείς το 13888 και η άρση φραγής γίνεται αυτόματα, μέσω αυτοματοποιημένου μηνύματος.",
                right: true
            }]
        }, {
            question_id: 2,
            question: "Οι πελάτες Cosmote One (μήπως αφορά μόνο τους Cosmote Business One;) καλώντας το 13888 διαθέτουν προσωποποιημένη εξυπηρέτηση:",
            answers: [{
                answer_number: "A",
                answer: "Με έναν προσωπικό εκπρόσωπο για να μιλούν πάντα με τον ίδιο.",
                right: false
            }, {
                answer_number: "B",
                answer: "Με δύο προσωπικούς εκπροσώπους, για ακόμη καλύτερη, εξειδικευμένη εξυπηρέτηση.",
                right: true
            }]
        }, {
            question_id: 3,
            question: "Ο εκπρόσωπος τεχνικής υποστήριξης στο 13888 μπορεί να κατευθύνει τον πελάτη για την επίλυση ενός προβλήματος που αντιμετωπίζει στη σύνδεση ή συσκευή του με τους εξής τρόπους:",
            answers: [{
                answer_number: "A",
                answer: "Μέσα από αναλυτικές, «βήμα-βήμα» τηλεφωνικές οδηγίες.",
                right: false
            }, {
                answer_number: "B",
                answer: "Μέσα από την υπηρεσία Ufixit, ο εκπρόσωπος μπορεί  πραγματοποιώντας videoκλήση να «δει» τις ενέργειες που απαιτούνται & να καθοδηγήσει τον πελάτη live.",
                right: false
            }, {
                answer_number: "C",
                answer: "Όλα τα παραπάνω",
                right: true
            }]
        }, {
            question_id: 4,
            question: "Το ιστορικό ενός πελάτη που δηλώνει βλάβη ή κάποιο άλλο πρόβλημα το γνωρίζει/ουν:",
            answers: [{
                answer_number: "A",
                answer: "Μόνο οι εκπρόσωποι του 13888.",
                right: false
            }, {
                answer_number: "B",
                answer: "Κάθε εκπρόσωπος της COSMOTE, σε οποιοδήποτε κανάλι εξυπηρέτησης.",
                right: true
            }]
        }, {
            question_id: 5,
            question: "Για την εξυπηρέτηση εταιρικών πελατών καλούμε:",
            answers: [{
                answer_number: "A",
                answer: "To 13888",
                right: false
            }, {
                answer_number: "B",
                answer: "To 13818",
                right: true
            }, {
                answer_number: "C",
                answer: "Οποιονδήποτε αριθμό από τους παραπάνω",
                right: false
            }]
        }];
    $scope.questions['stores'] = [
        {
            question_id: 1,
            question: "Πόσα είναι τα καταστήματα COSMOTE συνολικά;",
            answers: [{
                answer_number: "A",
                answer: "85",
                right: false
            }, {
                answer_number: "B",
                answer: "150",
                right: false
            }, {
                answer_number: "C",
                answer: "145",
                right: true
            }]
        }, {
            question_id: 2,
            question: "Πόσοι είναι οι εργαζόμενοι των καταστημάτων Cosmote πανελλαδικά; ",
            answers: [{
                answer_number: "A",
                answer: "680",
                right: false
            }, {
                answer_number: "B",
                answer: "1050",
                right: false
            }, {
                answer_number: "C",
                answer: "1258",
                right: true
            }]
        }, {
            question_id: 3,
            question: "Σε  πόσα από τα 145 καταστήματα COSMOTE μπορεί κανείς να απευθυνθεί σε έναν Business Expert, δηλαδή στον ειδικό εκπρόσωπο σε θέματα B2B;",
            answers: [{
                answer_number: "A",
                answer: "Σε 10 επιλεγμένα καταστήματα.",
                right: false
            }, {
                answer_number: "B",
                answer: "Σε 80, δηλαδή πάνω από το 50% των καταστημάτων.",
                right: true
            }]
        }, {
            question_id: 4,
            question: "Ποιο από τα παρακάτω προϊόντα τεχνολογίας μπορεί να βρει κάποιος σε ένα κατάστημα COSMOTE;",
            answers: [{
                answer_number: "A",
                answer: "Αξεσουάρ σταθερής & κινητής",
                right: false
            }, {
                answer_number: "B",
                answer: "Smartphones, Tablets & Laptops",
                right: false
            }, {
                answer_number: "C",
                answer: "Τηλεοράσεις",
                right: false
            }, {
                answer_number: "D",
                answer: "Ασύρματα Τηλέφωνα",
                right: false
            }, {
                answer_number: "E",
                answer: "Όλα τα παραπάνω",
                right: true
            }]
        }
    ];
    $scope.questions['it'] = [
        {
            question_id: 1,
            question: "Πόσοι Τεχνικοί Πεδίου κατέχουν υψηλή τεχνολογική κατάρτιση;",
            answers: [{
                answer_number: "A",
                answer: "Περισσότεροι από 500",
                right: false
            }, {
                answer_number: "B",
                answer: "Περισσότεροι από 2.000",
                right: true
            }, {
                answer_number: "C",
                answer: "Περισσότεροι από 1.000",
                right: false
            }]
        }, {
            question_id: 2,
            question: "Η πλειοψηφία των βλαβών επιλύεται εντός:",
            answers: [{
                answer_number: "A",
                answer: "Μερικών ωρών",
                right: false
            }, {
                answer_number: "B",
                answer: "Μια ημέρας",
                right: true
            }, {
                answer_number: "C",
                answer: "3 ημερών",
                right: false
            }]
        }, {
            question_id: 3,
            question: "Πόσα Τεχνικά Τμήματα Λειτουργιών Πεδίου (ΤΤΛΠ) υπάρχουν πανελλαδικά;",
            answers: [{
                answer_number: "A",
                answer: "51",
                right: false
            }, {
                answer_number: "B",
                answer: "73",
                right: true
            }, {
                answer_number: "C",
                answer: "98",
                right: false
            }]
        }
    ];
    $scope.questions['sitenapp'] = [
        {
            question_id: 1,
            question: "Το νέο cosmote.gr  site προσφέρει online υποστήριξη;",
            answers: [{
                answer_number: "A",
                answer: "Ναι, με άμεση συνομιλία με εκπρόσωπο μέσω του Online Chat.",
                right: false
            }, {
                answer_number: "B",
                answer: "Ναι, με δυνατότητα επικοινωνίας μέσω Online Chat, Call Back και Video Call .",
                right: true
            }, {
                answer_number: "C",
                answer: "Ναι, μόνο μέσω της υπηρεσίας Call Back που λειτουργεί Δευτέρα ως Παρασκευή, 08:00-23:00 και Σάββατο 08:00-20:00. ",
                right: false
            }]
        }, {
            question_id: 2,
            question: "Στο cosmote.gr site μπορείς να επικοινωνήσεις με εκπρόσωπο COSMOTE μέσω video κλήσης:",
            answers: [{
                answer_number: "A",
                answer: "Στα Ελληνικά και στα Αγγλικά.",
                right: false
            }, {
                answer_number: "B",
                answer: "Στα Ελληνικά, στα Αγγλικά και στη νοηματική.",
                right: true
            }]
        }, {
            question_id: 3,
            question: "Με το My COSMOTE Αpp έχεις τον έλεγχο του κινητού σου, εύκολα και απλά, ακόμα και χωρίς κωδικούς πρόσβασης.",
            answers: [{
                answer_number: "A",
                answer: "Ναι, υπάρχει αυτή η δυνατότητα.",
                right: true
            }, {
                answer_number: "B",
                answer: "Όχι, οι κωδικοί πρόσβασης είναι υποχρεωτικοί.",
                right: false
            }]
        }, {
            question_id: 4,
            question: "Τι από τα παρακάτω, ΔΕΝ μπορεί να κάνει κανείς, μέσω του My COSMOTE App; ",
            answers: [{
                answer_number: "A",
                answer: "Να δει το υπόλοιπό του κινητού του σε ομιλία, μηνύματα & ογκοχρέωση.",
                right: false
            }, {
                answer_number: "B",
                answer: "Να πληρώσει το λογαριασμό κινητής & σταθερής & να ενημερωθεί για το ύψος του & την ημερομηνία εξόφλησης.",
                right: false
            }, {
                answer_number: "C",
                answer: "Να δει τις κλήσεις του κινητού του εντός & εκτός παγίου.",
                right: false
            }, {
                answer_number: "D",
                answer: "Να μεταφέρει υπόλοιπο από το κινητό του σε άλλους αριθμούς COSMOTE Καρτοκινητής ή Καρτοσυμβολαίου.",
                right: false
            }, {
                answer_number: "E",
                answer: "Να αλλάξει αριθμό.",
                right: true
            }]
        }, {
            question_id: 5,
            question: "Πώς μπορεί κανείς να διαπιστώσει αν ο αριθμός κάποιας από τις επαφές του ανήκει στο δίκτυο της COSMOTE;",
            answers: [{
                answer_number: "A",
                answer: "Μέσα από το cosmote.gr.",
                right: false
            }, {
                answer_number: "B",
                answer: "Μέσα από το My COSMOTE App.",
                right: true
            }]
        }, {
            question_id: 6,
            question: "Μπορείς με το My COSMOTE App να κανονίσεις μια έξοδο για φαγητό, θέατρο, σινεμά ή διασκέδαση με όλη την οικογένεια;",
            answers: [{
                answer_number: "A",
                answer: "Ναι, μέσα από τις προσφορές του COSMOTE Deals for You",
                right: true
            }, {
                answer_number: "B",
                answer: "Ναι, μέσω εβδομαδιαίου διαγωνισμού",
                right: false
            }]
        }
    ];


    $scope.getRandomQuestion = function () {
        var question_array = $scope.questions[CategoryService.getCategory()];
        var index = Math.floor((Math.random() * question_array.length));
        question_array.slice(index, 1);
        $scope.question = question_array[index];
    };
    $scope.getRandomQuestion();
}]);
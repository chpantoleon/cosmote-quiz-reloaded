/**
 * Created by cpad on 28/4/2017.
 */
"use strict";
/**
 * @ngdoc function
 * @name cosmoteGameApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the cosmoteGameApp
 */
cosmote_game.controller("RegistrationCtrl", ["$scope", "ScoreService", "$http", "authentication", "URLService", "$location", function ($scope, ScoreService, $http, authentication, URLService, $location) {
    jQuery.support.cors = true;
    jQuery(".ote-icon").show();
    jQuery(document).ready(function () {
        jQuery("input, textarea").placeholder({customClass: "custom-placeholder"});
    });
    ScoreService.setPlaying(false);
    if (!authentication.isLogged()) {
        window.location.assign("/");
    }
    $scope.showFinalErrorModal = false;
    $scope.submitAgain = false;
    $scope.playerFirstName = "";
    $scope.playerLastName = "";
    $scope.playerAddress = "";
    $scope.playerMobile = "";
    $scope.playerEmail = ScoreService.getEmail();
    $scope.notValidMobile = function () {
        if (isNaN($scope.playerMobile) || $scope.playerMobile.length != 10) {
            return true;
        }
        return false;
    };
    $scope.validateInfo = function () {
        if ($scope.playerFirstName == "" || $scope.playerLastName == "" || $scope.playerAddress == "" || $scope.playerMobile == "") {
            $scope.showFinalErrorModal = true;
            return false;
        }
        if (isNaN($scope.playerMobile) || $scope.playerMobile.length != 10) {
            $scope.showFinalErrorModal = true;
            return false;
        }
        return true;
    };
    $scope.submitPersonalInfo = function () {
        var procceed = $scope.validateInfo();
        if (!procceed) return;
        var score = ScoreService.getScore();
        var email = ScoreService.getEmail();
        var url = URLService.getUrl() + "setUserDetails.php?callback=JSON_CALLBACK&firstname=" + $scope.playerFirstName + "&lastname=" + $scope.playerLastName + "&address=" + $scope.playerAddress + "&mobile=" + $scope.playerMobile + "&score=" + score + "&email=" + email + "&tok=" + tok;
        $.ajax({
            url: url,
            dataType: "json",
            success: function (response) {
                if (response.status === "success") {
                    ScoreService.setScore(0);
                    $scope.submitAgain = false;
                    window.location.assign("/#/finished");
                } else {
                    $scope.submitAgain = true;
                }
            },
            error: function (error) {
                $scope.submitAgain = true;
            }
        });
    };
    $scope.logout = function () {
        authentication.logout();
        $location.path("/");
    };
}]);
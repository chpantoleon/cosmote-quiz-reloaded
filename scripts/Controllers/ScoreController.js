/**
 * Created by cpad on 28/4/2017.
 */
"use strict";

cosmote_game.controller("ScoreCtrl", ["$scope", "ScoreService", "CategoryService", "authentication", function($scope, ScoreService, CategoryService, authentication) {
    jQuery(".ote-icon").show();
    jQuery(document).ready(function() {
        jQuery("input, textarea").placeholder({
            customClass: "custom-placeholder"
        });
    });
    if (!authentication.isLogged()) {
        window.location.assign("/#/");
    }
    $scope.score = ScoreService.getScore();
}]);
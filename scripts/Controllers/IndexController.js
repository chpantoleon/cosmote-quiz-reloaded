/**
 * Created by cpad on 28/4/2017.
 */
"use strict";

cosmote_game.controller("IndexCtrl", ["authentication", "$scope", "$http", "ScoreService", "CategoryService", "$location", function(authentication, $scope, $http, ScoreService, CategoryService, $location) {
    jQuery(".ote-icon").show();
    ScoreService.setScore(0);
    ScoreService.setPlaying(false);
    jQuery(document).ready(function() {
        jQuery("input, textarea").placeholder({customClass : "custom-placeholder"});
    });
    $scope.indexForm = {
        email : "",
        acceptedTerms : false
    };
    $scope.showErrorModal = false;
    $scope.showTermsModal = false;
    $scope.showInfoModal = false;
    CategoryService.resetCategoryStatus();
    $scope.formSubmit = function() {
        var email = $("#emailInput").val();
        if ($scope.isCosmoteEmail($scope.indexForm.email)) {
            if ($scope.indexForm.acceptedTerms) {
                ScoreService.setEmail(email);
                authentication.login();
                $scope.showInfoModal = true;
            } else {
                $scope.showErrorModal = true;
            }
        } else {
            if (!$scope.indexForm.acceptedTerms) {
                $scope.showErrorModal = true;
            }
            else{
                $scope.showErrorModal = true;
            }
        }
    };
    $scope.redirectToGame = function(){
        $location.path("/categories");
    };
    $scope.showTerms = function() {
        $scope.showTermsModal = true;
    };
    $scope.isCosmoteEmail = function(email) {
        var cosmote = /^([\w-]+(?:\.[\w-]+)*)@cosmote.gr/;
        var ote = /^([\w-]+(?:\.[\w-]+)*)@ote.gr/;
        var germanos = /^([\w-]+(?:\.[\w-]+)*)@germanos.gr/;
        return (cosmote.test(email) || ote.test(email) || germanos.test(email));
    };
}]);
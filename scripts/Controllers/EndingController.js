/**
 * Created by cpad on 28/4/2017.
 */

cosmote_game.controller('EndingCtrl', function($scope, authentication, $location) {
    $scope.logout = function() {
        authentication.logout();
        $location.path("/");
    };
});

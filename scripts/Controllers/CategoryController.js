/**
 * Created by cpad on 28/4/2017.
 */
"use strict";

cosmote_game.controller('CategoryCtrl', function ($scope, ScoreService, CategoryService, $location, authentication) {
    if (!authentication.isLogged()) {
        $location.path("/");
    }

    $scope.setCategory = function (value, index) {
        CategoryService.setCategory(value);
        $scope.categoryStatus = CategoryService.getCategoryStatus();
        $scope.categoryStatus[index] = true;
        CategoryService.setCategoryStatus($scope.categoryStatus);
        $location.path("/game");
    };

    $scope.categories = [
        {
            icon: '../../images/categories/24support.png',
            slug: '24support',
            label: '24ΩΡΗ ΕΞΥΠΗΡΕΤΗΣΗ'
        },
        {
            icon: '../../images/categories/stores.png',
            slug: 'stores',
            label: 'ΜΕΓΑΛΟ ΔΙΚΤΥΟ ΚΑΤΑΣΤΗΜΑΤΩΝ'
        },
        {
            icon: '../../images/categories/it.png',
            slug: 'it',
            label: 'ΕΞΕΙΔΙΚΕΥΜΕΝΟΙ ΤΕΧΝΙΚΟΙ'
        },
        {
            icon: '../../images/categories/sitenapp.png',
            slug: 'sitenapp',
            label: 'ΕΥΧΡΗΣΤΟ SITE & APP'
        }
    ];

    $scope.alreadyAnswered = CategoryService.getCategoryStatus();
});